create table if not exists users
(
    id bigint generated by default as identity (maxvalue 2147483647)
        constraint users_pkey
            primary key,
    hash_password varchar(255),
    name varchar(255),
    role varchar(255)
);

alter table users owner to postgres;

create unique index if not exists users_name_uindex
    on users (name);



create table if not exists numbers
(
    id bigint generated by default as identity
        constraint numbers_pkey
            primary key,
    is_used boolean,
    letters varchar(255)
        constraint uk_a7c6yj0gfu3c6e1ncnfbvyynq
            unique
);

alter table numbers owner to postgres;

INSERT INTO users (name, hash_password, role) VALUES ('Shamil','$2a$10$KzXyct44NCA1rb/tnTvSzOFm2G8/MkOUm/1LcRQ6MzebBZ6c6Ku7G','USER') ON CONFLICT (name) DO NOTHING ;
INSERT INTO users (name, hash_password, role) VALUES ('Anton', '$2a$10$KzXyct44NCA1rb/tnTvSzOFm2G8/MkOUm/1LcRQ6MzebBZ6c6Ku7G', 'USER') ON CONFLICT (name) DO NOTHING ;
INSERT INTO users (name, hash_password, role) VALUES ('Ivan', '$2a$10$KzXyct44NCA1rb/tnTvSzOFm2G8/MkOUm/1LcRQ6MzebBZ6c6Ku7G', 'USER') ON CONFLICT (name) DO NOTHING ;