package ru.garipov.cng.controllers;

import org.springframework.security.core.Authentication;
import ru.garipov.cng.services.CarNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

@RestController
public class CarNumberController {

    @Autowired
    CarNumberService carNumberService;

    @GetMapping("/number/random")
    public ResponseEntity<String> random(Authentication authentication)   {
        return ResponseEntity.ok(carNumberService.generateRandomCarNumber(authentication));
    }


    @GetMapping("/number/next")
    public ResponseEntity<String> next(Authentication authentication) {
        return ResponseEntity.ok(carNumberService.generateNextCarNumber(authentication));
    }

}
