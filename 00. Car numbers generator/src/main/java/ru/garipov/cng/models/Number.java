package ru.garipov.cng.models;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Data
@Entity
@Table(name = "numbers")
public class Number {

    public Number(String letters, Boolean isUsed) {
        this.letters = letters;
        this.isUsed = isUsed;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String letters;

    private Boolean isUsed;


}
