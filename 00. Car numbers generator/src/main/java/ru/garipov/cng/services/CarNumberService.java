package ru.garipov.cng.services;

import org.springframework.security.core.Authentication;

public interface CarNumberService {



    String generateRandomCarNumber(Authentication authentication);

    String generateNextCarNumber(Authentication authentication);

}
