package ru.garipov.cng.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.garipov.cng.models.Number;
import ru.garipov.cng.models.User;
import ru.garipov.cng.repositories.NumbersRepository;
import ru.garipov.cng.repositories.UsersRepository;

import java.util.*;
import java.util.concurrent.ExecutorService;

@Service
public class CarNumberServiceImpl implements CarNumberService {

    private final int STORAGE_SIZE = 1762271;
    //    private final int STORAGE_SIZE = 99;
    private final String REGION = " 116 RUS";

    @Autowired
    private NumbersRepository numbersRepository;

    @Autowired
    ExecutorService executorService;

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public String generateRandomCarNumber(Authentication authentication) {
        List<Number> numberList = numbersRepository
                .findByIsUsedFalse(PageRequest.of(new Random().nextInt(STORAGE_SIZE), 1));
        Number currentNumber = numberList.get(0);
        currentNumber.setIsUsed(true);
        numbersRepository.save(currentNumber);
        User currentUser = usersRepository.findByName(authentication.getName()).get();
        currentUser.setLastNumbers(currentNumber.getLetters());
        usersRepository.save(currentUser);

        return currentNumber.getLetters() + REGION;
    }

    @Override
    public String generateNextCarNumber(Authentication authentication) {
        User currentUser = usersRepository.findByName(authentication.getName()).get();
        System.out.println(currentUser.getName());
        String currentLetters = currentUser.getLastNumbers();
        System.out.println(currentLetters);

        long currentNumberId = numbersRepository.findByLetters(currentLetters).getId();
        System.out.println(currentNumberId);
        List<Number> numberList = numbersRepository
                .findByIdAfterAndIsUsedFalse(currentNumberId, PageRequest.of(0, 1));

        Number currentNumber = numberList.get(0);
        System.out.println(currentNumber.getLetters());
        currentNumber.setIsUsed(true);
        currentUser.setLastNumbers(currentNumber.getLetters());
        usersRepository.save(currentUser);
        return currentNumber.getLetters() + REGION;
    }

}
