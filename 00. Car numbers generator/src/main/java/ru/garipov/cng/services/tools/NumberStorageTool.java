package ru.garipov.cng.services.tools;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import ru.garipov.cng.models.Number;
import ru.garipov.cng.repositories.NumbersRepository;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.stream.IntStream;

@Service
public class NumberStorageTool {


    @Autowired
    private NumbersRepository numbersRepository;

    @Autowired
    private ExecutorService executorService;

    @Bean
    @Qualifier("number")
    public void numberStorageInit() {
        executorService.submit(() -> {
            List<Number> numbersList = new LinkedList<>();
            if (numbersRepository.findAll().size() == 0) {
                String[] alphabet = new String[]{"А", "В", "Е", "К", "М", "Н", "О", "Р", "С", "Т", "У", "Х"};
                Arrays.stream(alphabet).forEach(
                        firstLetter -> Arrays.stream(alphabet).forEach(
                                secondLetter -> Arrays.stream(alphabet).forEach(
                                        thirdLetter -> IntStream.range(1, 1000).forEach(
                                                nums -> numbersList.add(new Number(String.format("%s%03d%s%s",
                                                        firstLetter,
                                                        nums,
                                                        secondLetter,
                                                        thirdLetter),false)))
                                )
                        )
                );
                numbersRepository.saveAll(numbersList);
                System.out.println(">>>Numbers storage started");
            }
        });
    }
}
