package ru.garipov.cng.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.garipov.cng.models.User;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    Optional<User> findByName(String name);
}
