package ru.garipov.cng.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.garipov.cng.models.Number;
import ru.garipov.cng.models.User;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public interface NumbersRepository extends JpaRepository<Number, Long> {

    List<Number> findByIsUsedFalse(Pageable pageable);

    Number findByLetters(String letters);

    List<Number> findByIdAfterAndIsUsedFalse(long id, Pageable pageable);
}
